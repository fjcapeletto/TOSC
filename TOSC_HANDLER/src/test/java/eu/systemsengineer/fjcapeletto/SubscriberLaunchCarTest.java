package eu.systemsengineer.fjcapeletto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import eu.systemsengineer.fjcapeletto.model.CarsBase;
import eu.systemsengineer.fjcapeletto.msg.publisher.PublisherLaunchCar;
import eu.systemsengineer.fjcapeletto.msg.subscriber.SubscriberLaunchCar;
import eu.systemsengineer.fjcapeletto.model.Car;

public class SubscriberLaunchCarTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(SubscriberLaunchCarTest.class);
	static ApplicationContext springContext = new ClassPathXmlApplicationContext("FleetConfiguration.xml");
	static CarsBase carsBaseConfig = (CarsBase) springContext.getBean("carsbase-configuration");
	private static PublisherLaunchCar publisherLaunchCar;
	private static SubscriberLaunchCar subscriberLaunchCar;
	private static BrokerConfig brokerconfigIntance = BrokerConfig.getInstance();
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		brokerconfigIntance.start();
		publisherLaunchCar = new PublisherLaunchCar();
		publisherLaunchCar.connect(null);
		subscriberLaunchCar = new SubscriberLaunchCar();
		subscriberLaunchCar.connect(null);

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		publisherLaunchCar.closeConnection();
		subscriberLaunchCar.closeConnection();
	}

	@Test
	public void testLaunchCarOneCar() {
		try {
			ArrayList<Car> carsListToLaunch = new ArrayList<Car>();
			carsListToLaunch.add(carsBaseConfig.getFleetCars().get(7));
			publisherLaunchCar.publish(carsListToLaunch,90);
			subscriberLaunchCar.getMessageConsumer().setMessageListener(new MessageListener() {
				@Override
				public void onMessage(Message message) {
					LOGGER.debug("onMessage " + message);
					if (message instanceof ObjectMessage) {
						ObjectMessage objMsg = (ObjectMessage) message;
						try {
							List<?> listCarsLaunched = (List<?>) objMsg.getObject();
							Car track1 = (Car) listCarsLaunched.get(0);
							assertEquals(String.valueOf(7), String.valueOf(track1.getCarId()));
							assertEquals("Hyundai",track1.getCarName());
							assertEquals(8.0,track1.getLastSpeed(), 0.001);
							assertEquals(90,track1.getLastCourse(),0.001);
						} catch (JMSException e) {
							LOGGER.error("JMSException:" + e);
						}
					}
					
				}
			});
		} catch (JMSException e) {
			LOGGER.error("JMSException:" + e);
		}
	}

	@Test
	public void testLaunchCarMoreThanOneCar() {
		try {
			ArrayList<Car> carsListToLaunch = new ArrayList<Car>();
			carsListToLaunch.add(carsBaseConfig.getFleetCars().get(7));
			carsListToLaunch.add(carsBaseConfig.getFleetCars().get(1));
			carsListToLaunch.add(carsBaseConfig.getFleetCars().get(9));
			publisherLaunchCar.publish(carsListToLaunch,90);
			subscriberLaunchCar.getMessageConsumer().setMessageListener(new MessageListener() {
				@Override
				public void onMessage(Message message) {
					LOGGER.debug("onMessage " + message);
					if (message instanceof ObjectMessage) {
						ObjectMessage objMsg = (ObjectMessage) message;
						try {
							List<?> listCarsLaunched = (List<?>) objMsg.getObject();
							Car track1 = (Car) listCarsLaunched.get(0);
							assertEquals(String.valueOf(7), String.valueOf(track1.getCarId()));
							assertEquals("Hyundai",track1.getCarName());
							assertEquals(8.0,track1.getLastSpeed(), 0.001);
							assertEquals(90,track1.getLastCourse(),0.001);
							
							Car track2 = (Car) listCarsLaunched.get(1);
							assertEquals(String.valueOf(1), String.valueOf(track2.getCarId()));
							assertEquals("VW",track2.getCarName());
							assertEquals(25.0,track2.getLastSpeed(), 0.001);
							assertEquals(90,track2.getLastCourse(),0.001);

							Car track3 = (Car) listCarsLaunched.get(2);
							assertEquals(String.valueOf(9), String.valueOf(track3.getCarId()));
							assertEquals("Penelope",track3.getCarName());
							assertEquals(3.0,track3.getLastSpeed(), 0.001);
							assertEquals(90,track3.getLastCourse(),0.001);
							
							
						} catch (JMSException e) {
							LOGGER.error("JMSException:" + e);
						}
					}
					
				}
			});
		} catch (JMSException e) {
			LOGGER.error("JMSException:" + e);
		}
	}
	
	
}