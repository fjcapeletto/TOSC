package eu.systemsengineer.fjcapeletto.msg.publisher;

import javax.jms.JMSException;

import eu.systemsengineer.fjcapeletto.msg.UtilsMsg;

/**
 * Class containing the Apache ActiveMQ Publisher for the Handler Start Message
 * @author Fernando Jose Capeletto Neto
 */
public class PublisherHandlerStart extends Publisher {

	/**
	 * Prepare the connection, set the client and create the session. 
	 * @param brokerURL The URL for the broker service
	 * @throws JMSException 
	 */
	public void connect(String brokerURL) throws JMSException {
		super.connect(brokerURL, UtilsMsg.HANDLERSTART_TOPIC);
	}

	/**
	 * handlerStart Message Method. Alerts display that the Server was Started.
	 * In case of Display was started before Server, or due to the absence of the Server and its returns, Display situation will be reset when receives this message. 
	 * @throws JMSException
	 */
	public void publish() throws JMSException {
		super.publish("Server Started", true);
	}
}