package eu.systemsengineer.fjcapeletto.msg.subscriber;

import eu.systemsengineer.fjcapeletto.msg.UtilsMsg;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.jms.*;

/**
 * Class containing the Apache ActiveMQ Subscriber for the UpdateCarList Message
 * @author Fernando Jose Capeletto Neto
 */
public class Subscriber implements MessageListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(Subscriber.class);
	private static final String CLIENT_ID_PREFIX = UtilsMsg.getPrefixClient()+"subscriber-";
	private Connection connection;
	private MessageConsumer messageConsumer;

	public MessageConsumer getMessageConsumer() {
		return messageConsumer;
	}

	/**
	 * Prepare the conection, set the client and create the session. 
	 * @param brokerURL The URL for the broker service
	 * @throws JMSException
	 */	
	public void connect(String brokerURL, String topicName) throws JMSException {
		// create a Connection Factory		
		ActiveMQConnectionFactory connectionFactory = UtilsMsg.CONNECTIONFACTORY;
		try {
			String externalBroker = UtilsMsg.checkBrokerURL(brokerURL);
			connectionFactory.setBrokerURL(externalBroker);
		} catch (Exception e) {
			LOGGER.error("Exception: " + e);
			LOGGER.error("Default broker will be used: " + UtilsMsg.CONNECTIONFACTORY.getBrokerURL());
		}
		connectionFactory.setTrustAllPackages(true);
		
		// create a Connection
		connection = connectionFactory.createConnection();
		connection.setClientID(Subscriber.CLIENT_ID_PREFIX+topicName);


		// create a Session
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

		// create the Topic from which messages will be received
		Topic topic = session.createTopic(topicName);

		// create a MessageConsumer for receiving messages
		messageConsumer = session.createConsumer(topic);

		// start the connection in order to receive messages
		connection.start();
	}

	/**
	 * Close the connection 
	 * @throws JMSException
	 */
	public void closeConnection() throws JMSException {
		connection.close();
	}

	/**
	 * OnMessage Method.
	 * @param	message The message
	 */
	@Override
	public void onMessage(Message message) {
		LOGGER.debug("onMessage " + message);
		if (message instanceof ObjectMessage) {
			ObjectMessage objMsg = (ObjectMessage) message;
			try {
				LOGGER.info("Abstract method, shall be override by child classes: " +   objMsg.getObject() );
			} catch (JMSException e) {
				LOGGER.error("Error receiving message: " + e );
			}
		}
	}
}