package eu.systemsengineer.fjcapeletto.display;

import java.util.Timer;
import java.util.TimerTask;

import eu.systemsengineer.fjcapeletto.msg.config.NetworkLayerLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HeartBeatThread extends Thread {

	private static final Logger LOGGER = LoggerFactory.getLogger(HeartBeatThread.class);
	private RemindTask heartBeatTask;
	private	Timer heartBeattimer;
	// TOS Controller Instantiation
	private TOSDisplayController instanceController = TOSDisplayController.getInstance();


	HeartBeatThread() {
		heartBeattimer = new Timer();
		heartBeatTask = new RemindTask();
	}

	/**
	 * Returns the timer for the HeartBeat
	 * @return heartBeattimer
	 */
	public Timer getHeartBeattimer() {
		return heartBeattimer;
	}
	
	/**
	 * Starts the HeatBeat Thread
	 */
	@Override
	public void run() {
		heartBeattimer.schedule(heartBeatTask, NetworkLayerLoader.CONTROLLER_DELAYTIME_INMILLIS_FOR_HEARTBEAT);
		LOGGER.info("New HeartBeat Receiver Task Activated, Timeout in "+ NetworkLayerLoader.CONTROLLER_DELAYTIME_INMILLIS_FOR_HEARTBEAT + "ms");
	}

	/**
	 * Auxiliary Class containing the Task for the HeartBeat Thread. 
	 * @author Fernando Jose Capeletto Neto
	 */
	class RemindTask extends TimerTask {
		@Override
		/**
		 * Starts the HeatBeat Task, Set the HearBeating information to false after UtilsDisplay.CONTROLLER_TIMEOUT_INMILLIS_FOR_HEARTBEAT timeout
		 * If the HeartBeat msg is received in the TOSController, the timer concerning this task is cancelled and purget and a new one is created.
		 */
		public void run() {
			instanceController.setHeartBeating(false);
			LOGGER.info("Heat Beat Timeout reached. Server is down.");
		}

	}

}
