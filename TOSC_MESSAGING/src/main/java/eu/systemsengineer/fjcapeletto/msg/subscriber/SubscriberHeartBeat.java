package eu.systemsengineer.fjcapeletto.msg.subscriber;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import eu.systemsengineer.fjcapeletto.msg.UtilsMsg;

/**
 * Class containing the Apache ActiveMQ Subscriber for the Heart Beat Message
 * @author Fernando Jose Capeletto Neto
 */
public class SubscriberHeartBeat extends Subscriber {

	private static final Logger LOGGER = LoggerFactory.getLogger(SubscriberHeartBeat.class);

	/**
	 * Prepare the connection, set the client and create the session. 
	 * @param brokerURL The URL for the broker service
	 * @throws JMSException
	 */
	public void connect(String brokerURL) throws JMSException {
		super.connect(brokerURL,UtilsMsg.HEARTBEAT_TOPIC);
	}

	/**
	 * OnMessage Method. 
	 * When the message arrives, cast it to a Boolean object, but the arriving of the message itself already is a indicator. 
	 * @param	message The message
	 */
	@Override
	public void onMessage(Message message) {
		LOGGER.debug("onMessage " + message);
		if (message instanceof ObjectMessage) {
			ObjectMessage objMsg = (ObjectMessage) message;
			try {
				Boolean started = (Boolean) objMsg.getObject();
				LOGGER.info("Message for heartBeat consumed: " + started);
			} catch (JMSException e) {
				LOGGER.debug("Not a Track obj: " + e);
			}
		}
	}
}