package eu.systemsengineer.fjcapeletto.msg.subscriber;

import java.util.Iterator;
import java.util.List;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import eu.systemsengineer.fjcapeletto.model.Car;
import eu.systemsengineer.fjcapeletto.msg.UtilsMsg;

/**
 * Class containing the Apache ActiveMQ Subscriber for the LaunchCar Message
 * @author Fernando Jose Capeletto Neto
 */
public class SubscriberLaunchCar extends Subscriber {

	private static final Logger LOGGER = LoggerFactory.getLogger(SubscriberLaunchCar.class);
	/**
	 * Prepare the conection, set the client and create the session. 
	 * @param brokerURL The URL for the broker service
	 * @throws JMSException
	 */
	public void connect(String brokerURL) throws JMSException {
		super.connect(brokerURL,UtilsMsg.LAUNCHCAR_TOPIC);
	}

	/**
	 * OnMessage Method. 
	 * When the message arrives, cast it to a Car object. 
	 * @param	message The message
	 */
	@Override
	public void onMessage(Message message) {
		LOGGER.debug("onMessage " + message);
		if (message instanceof ObjectMessage) {
			LOGGER.info("Received Object Message (List<Car> Type): [" + message + "]"+"[content:{"+message.toString()+"}");
			ObjectMessage objMsg = (ObjectMessage) message;
			try {
				List<?> tracksList = (List<?>) objMsg.getObject();
				for (Iterator<?> iterator = tracksList.iterator(); iterator.hasNext();) {
					Car car = (Car) iterator.next();
					LOGGER.info("Message for launch List of Cars consumed: Launching " + car.getCarName());					
				}
			} catch (JMSException e) {
				LOGGER.debug("Not a Track obj: " + e);
			}
		}

		
	}
}