# Terminal Operating System Challenge

Project-Challenge made by Fernando Jose Capeletto Neto to the application to TBA Group

Release Notes:
- version 1.1.0 : 
    .   PWFJCN-15: Fixing SonarQube issues for TOCS
    .   PWFJCN-59: Enabled Ignored UT from old versions
- version 1.0.2 :
    .   PWFJCN-12 : Adding sonarqube configuration for mvn properties:read-project-properties clean install sonar:sonar
    .   PWFJCN-52: Fix pipeline job for TOSC and added nexus.systemsengineer.eu to the repository.