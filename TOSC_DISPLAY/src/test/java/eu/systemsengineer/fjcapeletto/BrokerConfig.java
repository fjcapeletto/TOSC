package eu.systemsengineer.fjcapeletto;

import org.apache.activemq.broker.BrokerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class BrokerConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(BrokerConfig.class);
    private static BrokerConfig instance = null;
    private static final BrokerService broker = new BrokerService();

    public BrokerConfig() {
        // configure the broker
        try {
            broker.addConnector("tcp://localhost:61616");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static BrokerConfig getInstance() {
        if(instance == null){
            instance = new BrokerConfig();
        }
        return instance;

    }

    public void start() throws Exception {
        if (!broker.isStarted()) {
            broker.start();
        } else {
            LOGGER.info("ActiveMQ Broker already started");
        }
    }
}
