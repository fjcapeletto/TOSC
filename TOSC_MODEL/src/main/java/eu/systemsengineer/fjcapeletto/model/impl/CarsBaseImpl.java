package eu.systemsengineer.fjcapeletto.model.impl;

import java.util.ArrayList;
import java.util.List;

import eu.systemsengineer.fjcapeletto.model.Car;
import eu.systemsengineer.fjcapeletto.model.CarsBase;

/**
 * Class representing the Fleet of Cars for a given Terminal.
 * The Fleet have a max limit for cars and a list of cars.
 * Configured through Spring Injection by the bean 'carsbase-configuration'  
 * @author Fernando Jose Capeletto Neto
 */


public class CarsBaseImpl implements CarsBase {
	private List<Car> fleetCars =  new ArrayList<>();
	private int maxCars;
	
	/**
	 * Returns the max numbers of cars for the Terminal. 
	 * @return max numbers of cars for the Terminal.
	 */
	public int getMaxCars() {
		return maxCars;
	}

	/**
	 * Set the max numbers of cars for the Terminal. 
	 * Used for Spring Injection.
	 * @param	maxCars	max numbers of cars for the Terminal.
	 */
	public void setMaxCars(int maxCars) {
		this.maxCars = maxCars;
	}

	/**
	 * Set the fleet of cars for the Terminal. 
	 * Used for Spring Injection.
	 * @param carCollection	a list of cars
	 */
	public void setFleetCars(List<Car> carCollection) {
		this.fleetCars = carCollection;
	}

	/**
	 * Returns the collection of cars representing the fleet of cars for the Terminal. 
	 * @return	a list of cars
	 */
	public List<Car> getFleetCars() {
		return this.fleetCars;
	}
}
