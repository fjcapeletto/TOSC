package eu.systemsengineer.fjcapeletto.model.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Class containing global definitions and utilities methods for use in whole project. 
 * Concentrate all Spring beans and provides it to the other classes.
 * @author Fernando Jose Capeletto Neto
 */
public class TerminalLoader {

	private TerminalLoader() {
	}

	// Beans Configurations
	// XML Configuration Files and its Config Beans
	private static ApplicationContext springContext = new ClassPathXmlApplicationContext("TerminalConfiguration.xml");
	private static TerminalConfiguration terminalConfig = (TerminalConfiguration) springContext.getBean("terminal-configuration");
	
	// Concentrates all the properties injected by beans in utilities attributes ...
	
	//.. for TOS Terminal Configurations
	public static final int TERMINALHORIZONTALSIZE = terminalConfig.getTerminalHorizontalSize();
	public static final int TERMINALVERTICALSIZE = terminalConfig.getTerminalVerticalSize();
	public static final int TERMINALSTARTHORIZONTALPOSITION = terminalConfig.getTerminalStartPosition();
	public static final int TERMINALBORDER = terminalConfig.getTerminalBorder();
	public static final int CARMARGIN = terminalConfig.getCarMargin();


}
