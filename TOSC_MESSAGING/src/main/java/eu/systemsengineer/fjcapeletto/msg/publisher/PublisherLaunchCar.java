package eu.systemsengineer.fjcapeletto.msg.publisher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.jms.JMSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import eu.systemsengineer.fjcapeletto.model.Car;
import eu.systemsengineer.fjcapeletto.msg.UtilsMsg;

/**
 * Class containing the Apache ActiveMQ Publisher for the LaunchCar Message
 * @author Fernando Jose Capeletto Neto
 */
public class PublisherLaunchCar extends Publisher{

	private static final Logger LOGGER = LoggerFactory.getLogger(PublisherLaunchCar.class);
	private List<Car> tracksList = Collections.synchronizedList(new ArrayList<>());

	/**
	 * Prepare the connection, set the client and create the session. 
	 * @param brokerURL The URL for the broker service
	 * @throws JMSException 
	 */
	public void connect(String brokerURL) throws JMSException {
		super.connect(brokerURL, UtilsMsg.LAUNCHCAR_TOPIC);
	}

	/**
	 * LaunchCar Message Method. Prepare Car Object and sent it.
	 * Takes the parameters from Spring injection.
	 * The only two variable parameters are the id and the course for the car. 
	 * @param	tracksList The list of cars to be launched 
	 * @param	angleInDegrees The initial course in degrees 
	 * @throws JMSException
	 */
	public void publish(List<Car> tracksList, double angleInDegrees) throws JMSException {
		for (Iterator<Car> iterator = tracksList.iterator(); iterator.hasNext();) {
			Car car = iterator.next();
			car.addCourse((float) angleInDegrees);
			this.tracksList.add(car);
			LOGGER.info("Publishing car id " + car.getCarId() + ": Speed: " + car.getLastSpeed() +
					" units/cycle [Lat/Long]:(" + car.getLastLatitude()+","+car.getLastLongitude() + ") "
					+ "Course: " + car.getLastCourse() + " Color: " + car.getCarColor());
		}
		super.publish("Launch Car", this.tracksList);
	}
}