package eu.systemsengineer.fjcapeletto.msg.subscriber;

import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.systemsengineer.fjcapeletto.model.Car;
import eu.systemsengineer.fjcapeletto.msg.UtilsMsg;

/**
 * Class containing the Apache ActiveMQ Subscriber for the UpdateCarList Message
 * @author Fernando Jose Capeletto Neto
 */
public class SubscriberUpdateCarList extends Subscriber {

	private static final Logger LOGGER = LoggerFactory.getLogger(SubscriberUpdateCarList.class);

	/**
	 * Prepare the conection, set the client and create the session. 
	 * @param brokerURL The URL for the broker service
	 * @throws JMSException
	 */	
	public void connect(String brokerURL) throws JMSException {
		super.connect(brokerURL,UtilsMsg.UPDATECARMAP_TOPIC);
	}

	/**
	 * OnMessage Method. 
	 * When the message arrives, cast it to a List of Cars object. 
	 * @param	message The message
	 */
	@Override
	public void onMessage(Message message) {
		LOGGER.debug("onMessage " + message);
		if (message instanceof ObjectMessage) {
			ObjectMessage objMsg = (ObjectMessage) message;
				try {
					Map<Integer, Car> launchedCarsMap = (Map<Integer, Car>) objMsg.getObject();
					LOGGER.info("Consumed launchedCarsMap obj : Received a Map of  " +  launchedCarsMap.size() + "launched car(s)");
				} catch (JMSException e2) {
					LOGGER.debug("Nor a List<Track> obj " + e2);
				}
		}
	}
}